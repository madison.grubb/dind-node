#!/bin/bash

MAJOR_VERSIONS=("$1" "$2" "$3")

# get node, npm, and yarn versions
NODE_VERSIONS="$(curl -s https://nodejs.org/dist/ | grep "<a href=\"v" | awk -F '<a href="v*' '{ print $2 }' | awk -F '/' '{ print $1 }' | sort -rV)"
NPM_VERSION="$(npm view npm --version)"
YARN_VERSION="$(yarn -v)"

# Git Credential Setup
if [ "$4" == "--git" ] || [ "$4" == "-g" ]; then
  echo "Setting up git credentials..."
  mkdir -p ~/.ssh
  echo -e "Host gitlab.com\\n\\tStrictHostKeyChecking no\\n\\tLogLevel ERROR\\n" >> ~/.ssh/config
  git config --global user.email "$GIT_EMAIL"
  git config --global user.name "$GIT_USERNAME"
  echo -e "$ID_RSA" > ~/.ssh/id_rsa
  chmod 0400 ~/.ssh/id_rsa
  git remote set-url origin git@gitlab.com:"$CI_PROJECT_PATH"
  echo "Git credentials configured."
  git checkout master
  git reset --hard origin/master
  echo "master checked out."
fi

# remove node versions file
rm node_versions.csv

# add a line for each version
for VERSION in "${MAJOR_VERSIONS[@]}"; do
  LATEST_NODE_VERSION="$(echo "$NODE_VERSIONS" | grep "^$VERSION.*" | head -n 1)"
  echo "$LATEST_NODE_VERSION,$NPM_VERSION,$YARN_VERSION" >> node_versions.csv
done

# push changes if there are any
if [ "$4" == "--git" ] || [ "$4" == "-g" ]; then
  if ! git diff-index --quiet HEAD --; then
    git add node_versions.csv
    git commit -m "Updated node_versions.csv with the latest release via ci"
    git push origin master
  fi
fi
