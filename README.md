# Docker in Docker - Node

[Docker in Docker](https://hub.docker.com/_/docker) + [Nodejs](https://hub.docker.com/_/node) + [Nodejs Build Tools](https://hub.docker.com/_/node).

[This project deploys to docker hub](https://hub.docker.com/r/madisongrubb/dind-node).

## image tags:

* 8
* 10
* 12
* latest (always the newest version tag of this image)

## Adding Versions

Peruse the [official node builds](https://nodejs.org/dist/). Add the desired version to the node_versions.csv.

  1. The first entry should be the full version of nodejs to include
  2. The second entry is the version of npm to include
  3. The third entry is the version of yarn to include.

Run the image updater script to build and test the new image:

```
bash build.sh -b
```

Commit your changes, update the gitlab-ci jobs, and make a pull request.
